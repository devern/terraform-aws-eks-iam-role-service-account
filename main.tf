terraform {
  required_providers {
    kubernetes = {
      version = "~> 1.13.3"
    }
  }
}

data "aws_caller_identity" "current" {}

locals {
  oidc_issuer_domain = replace(var.cluster_oidc_issuer_url, "https://", "")
  role_name = var.role_name != "" ? var.role_name : var.name
}

data "aws_iam_policy_document" "service_account_assume_role" {
  statement {
    principals {
      type = "Federated"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:oidc-provider/${local.oidc_issuer_domain}"]
    }
    actions = [
      "sts:AssumeRoleWithWebIdentity"
    ]
    condition {
      test     = "StringEquals"
      variable = "${local.oidc_issuer_domain}:sub"

      values = [
        "system:serviceaccount:${var.namespace}:${var.name}"
      ]
    }
  }
}

resource "aws_iam_role" "role" {
  name = local.role_name
  force_detach_policies = true
  assume_role_policy = data.aws_iam_policy_document.service_account_assume_role.json
  tags = var.role_tags
}

resource "kubernetes_service_account" "service_account" {
  metadata {
    name = var.name
    namespace = var.namespace
    annotations = {
      "eks.amazonaws.com/role-arn" = aws_iam_role.role.arn
    }
  }
  automount_service_account_token = var.automount_service_account_token
}

resource "aws_iam_policy" "policy" {
  count       = var.policy_json != "" ? 1 : 0
  name        = var.policy_name != "" ? var.policy_name : local.role_name
  description = "Policy used by the role ${aws_iam_role.role.name}"

  policy = var.policy_json
}

resource "aws_iam_role_policy_attachment" "policy_attachment" {
  count       = var.policy_json != "" ? 1 : 0
  role       = aws_iam_role.role.name
  policy_arn = aws_iam_policy.policy[0].arn
}
